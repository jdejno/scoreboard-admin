/**
 * Data Models
 */

export interface ITournament {
  _id: string
  title: string
  organizers?: Array<{
    name: string
    company?: string
    email?: string
    phoneNumber?: string
    website?: string
  }>
  dates: Array<Date>
  location: {
    name: string
    city: string
    state: string
    address: string
  }
  sport: "Volleyball"
  divisions: Array<IDivision>,
  image: string
  status: string
}


export interface IDivision {
  _id: string
  level: string
  gender: "Men" | "Women" | "Coed"
  teams?: Array<ITeam>
  pools?: Array<IPool>
}

export interface IPool {
  _id: string
  title: string
  division: string
  teams: Array<ITeam | string>
  location?: { gym?: string, court?: string }
  format?: { gameScores?: Array<number> }
  matches?: Array<string | IMatch>
  startTime?: Date
}


export interface ITeam {
  _id: string
  title: string
  members?: Array<IPlayer>
  tournaments?: Array<string>
  isTeam?: boolean
}

export interface IPlayer {
  _id: string
  firstName: string
  lastName: string
  position?: string

}

export interface IMatch {
  _id: string
  teams: Array<{ _id: string, title: string }>
  refTeam: { _id: string, title: string }
  scheduledTime?: Date
  startTime?: Date
  location?: string
  court?: string
  games?: Array<IGame>

}

export interface IGame {
  _id: string
  teams: Array<string>
  final: boolean
  scores?: Array<IScores>
}

export interface IScores {
  timestamp?: Date
  score: Array<{
    team: string
    points: number
  }>
}


/**
 * View Models
 */

export interface ITimeGroupListView {
  present: {
    listItems: Array<ITournament>,
    title: string
  },
  future: {
    listItems: Array<ITournament>,
    title: string
  },
  past: {
    listItems: Array<ITournament>,
    title: string
  },
}
