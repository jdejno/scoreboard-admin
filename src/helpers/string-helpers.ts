import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

/**
 * String Helpers Class that will provide our string utilities such as sanitization
 */
@Injectable()
export class StringHelpers {

  /**
   * Cleans a string from all of the characters not allowed in regex
   * @param {string} dirtyString
   * @returns {string}
   */
  cleanStringForRegex(dirtyString: string): string {
    return dirtyString.replace(/[|&;$%@"<>()+,]/g, "");
  }

  /**
   * Cleans a string and then tests a regex patter before returning a boolean
   * @param {string} query
   * @param {string} str
   * @param {string} regexOptions
   * @returns {boolean}
   */
  testRegex(query: string, str: string, regexOptions: string = 'ig'): boolean {
    return new RegExp(this.cleanStringForRegex(query), regexOptions).test(str);
  }

}
