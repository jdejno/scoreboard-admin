import {Component} from "@angular/core";
import {AlertController, NavController} from "ionic-angular";
import {HomePage} from "../home/home";
import {SocialService} from "../../providers/social-service";
import {UserService} from "../../providers/user-service";

@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class LoginPage {

  constructor(private _alertController: AlertController,
              private _socialService: SocialService,
              private _navController: NavController,
              private _userService: UserService) {
  }

  /**
   * Click handler for the login button.
   */
  login() {
    this._socialService.fbLogin()
      .then(res => {
        this._userService.authenticateUser(res.authResponse.accessToken)
          .subscribe(() => {
            // Login was a success! Let's enter the application.
            this._enterApp();
          });
      })
      .catch(err => {
        // Login failed... let's tell the user.
        let alert = this._alertController.create({
          title: 'Login Failed',
          subTitle: 'Your login attempt to Facebook failed.',
          buttons: ['Close']
        });
        alert.present();
      });
  }

  /**
   * Enter the app on the home page for now.
   * We will likely want to do things like open to a list of the admin's tournaments or whatnot.
   * @private
   */
  private _enterApp() {
    this._navController.setRoot(HomePage, {animate: true, direction: 'forward'});
  }

}
