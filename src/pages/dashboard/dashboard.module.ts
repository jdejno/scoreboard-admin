import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {DashboardPage} from './dashboard';
import {DashboardService} from '../../providers/dashboard-service';
import {DataService} from '../../providers/data-service';
import {DashboardSidebarResultsComponent} from './dashboard-sidebar-results/dashboard-sidebar-results';
import {DashboardDivisionComponent} from './dashboard-division/dashboard-division';
import {PipeModule} from '../../pipes/pipe.module';

@NgModule({
  declarations: [
    DashboardPage,
    DashboardSidebarResultsComponent,
    DashboardDivisionComponent
  ],
  imports: [
    IonicPageModule.forChild(DashboardPage),
    PipeModule
  ],
  providers: [
    DashboardService,
    DataService
  ]
})
export class DashboardPageModule {}
