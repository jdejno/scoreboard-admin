import {Component, QueryList, ViewChild} from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';
import {DashboardSidebarResultsComponent} from './dashboard-sidebar-results/dashboard-sidebar-results';
import {DashboardDivisionComponent} from './dashboard-division/dashboard-division';
import {ITournament} from '../../../models/models';
import {SidebarService} from '../../providers/sidebar-service';
import {TournamentService} from '../../providers/tournament-service';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  
  public tournament: ITournament;
  
  //#region child views
  @ViewChild(DashboardSidebarResultsComponent)
  public sidebarComponent: DashboardSidebarResultsComponent;
  @ViewChild(DashboardDivisionComponent)
  public divisionComponents: QueryList<DashboardDivisionComponent>;
  
  //#endregion
  
  constructor(private _navParams: NavParams,
              private _tournamentService: TournamentService,
              private _sidebarService: SidebarService) {}
  
  ionViewWillEnter() {
    this._tournamentService.getTournament().subscribe(tournament => {
      this.tournament = tournament;
      this.sidebarComponent.initializeComponent(this.tournament._id);
    });
    this._sidebarService.hideSidebar();
    //this.divisionComponents.forEach(component => component.initializeComponent());
  }
  
  ionViewWillUnload() {
    this.sidebarComponent.destroyComponent();
    //this.divisionComponents.forEach(component => component.disposeComponent());
  }
  
}
