import {Component, Input} from '@angular/core';
import {IDashboardDivision} from '../../../providers/dashboard-service';

/**
 * Generated class for the DashboardDivisionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'dashboard-division',
  templateUrl: 'dashboard-division.html'
})
export class DashboardDivisionComponent {

  @Input('divisionId')
  public tournamentId: string;
  public division: IDashboardDivision;

  constructor() {
  }

  public initializeComponent() {

  }

  public disposeComponent() {

  }

}
