import {Component} from '@angular/core';
import {DashboardService, IDashboardDivisionResults} from '../../../providers/dashboard-service';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'dashboard-sidebar-results',
  templateUrl: 'dashboard-sidebar-results.html'
})
export class DashboardSidebarResultsComponent {
  
  private _tournamentId: string;
  public results: Array<IDashboardDivisionResults>;
  public loadError: boolean;
  
  //used to retry load on refresh
  private _retryLoadResultsObservable: Subject<void>;
  
  constructor(private _dashboardService: DashboardService) {
  }
  
  /**
   * Called from parent component to stay in sync with lifecycle events.
   */
  public initializeComponent(tournamentId: string) {
    this._tournamentId = tournamentId;
    this._dashboardService.initializeTeamResultsSubject(this._tournamentId)
    //set up retry to execute when clicking refresh icon. TODO: revisit this.
    //   .retryWhen(errors => {
    //       this._retryLoadResultsObservable = new Subject<void>();
    //       return errors.switchMap(() => this._retryLoadResultsObservable);
    //     }
    //   )
      .subscribe(results => this.results = results,
        err => console.log('Failed to get results.')
      )
    
  }
  
  /**
   * Called from parent component to unsubscribe from dashboard service.
   */
  public destroyComponent() {
    this._dashboardService.disposeSubjects();
  }
  
  /**
   * Retry loading results.
   */
  public retryLoadResults() {
    if (this._retryLoadResultsObservable) {
      this._retryLoadResultsObservable.next();
    }
  }
  
}
