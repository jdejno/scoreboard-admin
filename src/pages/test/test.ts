import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { ITournament } from '../../../models/models';
import { DataService } from '../../providers/data-service';
import { Socket } from 'ng-socket-io';

enum Model {
  Tournament,
  Division,
  Pool,
  Team
}



@IonicPage()
@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class TestPage {
  
  private _currId: string;
  private _currModelType: Model;
  
  public tournaments$: Observable<Array<ITournament>>;
  public data$: Observable<any>;
  public currTournament: ITournament;
  
  constructor(private _dataService: DataService,
              private _socket: Socket) {
    this.tournaments$ = this._dataService.getTournamentsInfo();
    this._socket.connect();
    this._socket.on('refresh', () => this.refreshData())
  }
  
  /**
   * Get the JSON object of the tournament.
   * @param {string} tournamentId
   */
  public onTournamentSelect(tournamentId: string) {
    this._currId = tournamentId;
    this._currModelType = Model.Tournament;
    this.data$ = this._dataService.getTournamentAllData(tournamentId).do(tournament => this.currTournament = tournament);
  }
  
  public onPoolSelect(poolId: string) {
    this._currId = poolId;
    this._currModelType = Model.Pool;
    this.data$ = this._dataService.getPool(poolId);
  }
  
  /**
   * Refresh the current data in view.
   */
  public refreshData() {
    if (this._currId) {
      switch(this._currModelType) {
        case Model.Tournament:
          this.onTournamentSelect(this._currId);
          break;
        case Model.Pool:
          this.onPoolSelect(this._currId);
          break;
      }
    }
  }
  
  ionViewWillLeave() {
    this._socket.disconnect();
  }
  
}
