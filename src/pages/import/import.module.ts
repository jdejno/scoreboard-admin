import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ImportPage} from './import';
import {PipeModule} from '../../pipes/pipe.module';
import {STATES, states} from '../../providers/value-providers/states';
import {FilesProvider} from '../../providers/files-service';
import {ComponentsModule} from '../../components/components.module';
import {GameFormatFormComponent} from '../../components/game-format-form/game-format-form';

@NgModule({
  declarations: [
    ImportPage,
  ],
  imports: [
    IonicPageModule.forChild(ImportPage),
    PipeModule,
    ComponentsModule
  ],
  providers: [
    {provide: STATES, useValue: states},
    FilesProvider
  ],
  entryComponents: [GameFormatFormComponent]
})
export class ImportPageModule {
}
