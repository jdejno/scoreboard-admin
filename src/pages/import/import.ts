import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {
  AlertController, FabContainer, Loading, LoadingController, LoadingOptions, Modal, ModalController, NavController,
  ToastController
} from 'ionic-angular';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {STATES} from '../../providers/value-providers/states';
import {FilesProvider} from '../../providers/files-service';
import _first from 'lodash/first';
import _each from 'lodash/each';
import {DataService} from '../../providers/data-service';
import {UserService} from '../../providers/user-service';
import {Observable} from 'rxjs/Observable';
import {ITournament} from '../../../models/models';
import {SidebarService} from '../../providers/sidebar-service';
import {TournamentService} from '../../providers/tournament-service';

@Component({
  selector: 'page-import',
  templateUrl: 'import.html'
})
export class ImportPage implements OnInit {
  
  //public members
  public tournamentForm: FormGroup;
  public states: Array<string>;
  
  //view children
  @ViewChild('fileInput')
  public nativeFileInput: ElementRef;
  @ViewChild('imageInput')
  public nativeImageInput: ElementRef;
  @ViewChild('menuFab')
  public menuFab: FabContainer;
  
  //template variables
  public inputFileName: string;
  public inputImageName: string;
  public submitButtonColor: string;
  public dateInputMaxDate: string;
  public minEndDate: string;
  
  
  //#endregion
  
  //#region Ctor
  
  constructor(private _alertCtrl: AlertController,
              private _fb: FormBuilder,
              private _toastCtrl: ToastController,
              private _loaderCtrl: LoadingController,
              private _modalCtrl: ModalController,
              private _filesService: FilesProvider,
              private _dataService: DataService,
              private _userService: UserService,
              private _navCtrl: NavController,
              private _sidebarService: SidebarService,
              private _tournamentService: TournamentService,
              @Inject(STATES) private _states: Array<string>) {
  }
  
  ngOnInit() {
    this._instantiateViewDefaults();
    this._instantiateTournamentForm();
  }
  
  ionViewWillEnter() {
    //hide sidebar when on import page.
    this._sidebarService.hideSidebar();
  }
  
  //#endregion
  
  //#region Public Methods
  
  /**
   * Submit tournament form.
   * If tournamentForm is valid     =>  Send to server.
   * If tournamentForm is not valid =>  Display alert pop up.
   */
  public submitForm() {
    this.menuFab.close();
    //if tournamentForm is valid  => POST to server.
    if (this.tournamentForm.valid) {
      
      //present loader
      let loader = this._getAndPresentLoader();
      
      this._getTournamentFormData(this.tournamentForm).subscribe(form => {
        this._filesService.postTournamentData(form).subscribe(
          results => {
            loader.dismiss();
            let tournament = Array.isArray(results.tournaments) ? results.tournaments[0] : results.tournaments;
            this._handleSuccessfulImportEvent(tournament);
          },
          () => {
            loader.dismiss();
            this._presentImportFailureAlert();
          }
        )
      })
      
    } else {
      this._presentRequiredFailedAlert();
    }
  }
  
  /**
   * GETs CSV Tournament Template from server to download by client.
   */
  public exportCsvTemplate() {
    this._filesService.downloadTournamentTemplateCSV('Tournament-Template.csv').subscribe(
      () => console.log('CSV Template exported.'),
      err => console.error(JSON.stringify(err))
    )
  }
  
  /**
   * Workaround to use native file input with ionic button.
   * @param event
   */
  public onFileInput(event) {
    event.preventDefault();
    this.nativeFileInput.nativeElement.click();
  }
  
  /**
   * When file changes for native HTML5 file input, display it in our view and add it to FormControl.
   */
  public onNativeFileInput() {
    let files = this.nativeFileInput.nativeElement.files;
    if (!files.length)
      return;
    
    let file = _first(files);
    this.inputFileName = file.name;
    this.tournamentForm.controls['tournamentFile'].setValue(file, {emitModelToViewChange: false});
  }
  
  /**
   * Workaround to use native image input with ionic button.
   * @param event
   */
  public onImageInput(event) {
    event.preventDefault();
    this.nativeImageInput.nativeElement.click()
  }
  
  /**
   * When file changes for native HTML5 file input, display it in our view and add it to FormControl.
   */
  public onNativeImageInput() {
    let files = this.nativeImageInput.nativeElement.files;
    if (!files.length)
      return;
    
    let file = _first(files);
    this.inputImageName = file.name;
    this.tournamentForm.controls['tournamentImage'].setValue(file, {emitModelToViewChange: false});
  }
  
  //#endregion
  
  
  //#region Private Helpers
  
  //#region Initializer(s)
  
  /**
   * Instantiate the view model defaults
   * @private
   */
  private _instantiateViewDefaults() {
    let now = new Date();
    this.dateInputMaxDate = new Date(now.getFullYear() + 5, now.getMonth(), now.getDate()).toISOString();
    this.states = this._states;
    this.submitButtonColor = 'danger';
  }
  
  /**
   * Instantiate the FormGroup for tournament info.
   * Set up subscription for status changes to tournamentForm to react to.
   * @private
   */
  private _instantiateTournamentForm(): void {
    this.tournamentForm = this._fb.group({
      tournamentTitle: ['', Validators.required],
      tournamentFile: ['', Validators.required],
      tournamentImage: '',
      dates: this._fb.group({
        start: ['', Validators.required],
        end: ['', Validators.required],
      }),
      location: this._fb.group({
        name: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required],
        address: '',
      }),
      format: this._fb.group({
        gameScores: this._fb.array([])
      })
    });
    
    //dates.end control update for min date based on dates.start
    this.tournamentForm.get('dates.start').valueChanges.subscribe(
      date => {
        this.tournamentForm.get('dates.end').patchValue(date, {emitModelToViewChange: true});
        this.minEndDate = date;
      }
    );
    
    //toggle FabList when FormGroup is VALID
    this.tournamentForm.statusChanges.subscribe(
      status => {
        if (status === 'VALID') {
          this.submitButtonColor = 'secondary';
          if (!this.menuFab._listsActive) {
            this.menuFab.toggleList();
          }
        } else {
          if (this.menuFab._listsActive) {
            this.menuFab.close();
          }
          this.submitButtonColor = 'danger';
        }
      },
      err => console.log(err)
    );
    
  };
  
  //#endregion
  
  
  //#region Form Helpers
  
  /**
   * Create Form Data from tournamentForm
   * @desc will stringify all the values added, which should be run through JSON.parse on the server.
   *  This allows us to send nested object data to the server alongside files (within FormData api)
   * @param {FormGroup} tournamentForm
   * @returns {FormData} - form data to send to the server. Includes metadata, imageFile, and tournamentFile.
   * @private
   */
  private _getTournamentFormData(tournamentForm: FormGroup): Observable<FormData> {
    let form = new FormData();
    _each(this.tournamentForm.controls, (control, name) => {
      if (name === 'tournamentFile' || name === 'tournamentImage') {
        form.append(name, control.value, control.value.name);
      } else {
        form.append(name, JSON.stringify(control.value));
      }
    });
    
    return this._userService.getUser().map(user => {
      form.append('creator', JSON.stringify(user));
      return form;
    })
    
  }
  
  /**
   * Update the gameScores FormArray
   * @param {FormArray} scoresFormArray
   */
  public updateScoreFormArray(scoresFormArray: FormArray) {
    let formats = this.tournamentForm.get('format') as FormGroup;
    formats.setControl('gameScores', scoresFormArray);
  }
  
  //#endregion
  
  
  //#region Success/Failure Handlers
  
  /**
   * Event Handlers following a successful tournament import/creation.
   * @desc
   *  - switches submit button color
   *  - disables tournament FormGroup
   *  - presents toast for success
   *  - modal pops up for tournament creation review
   *  - Emits Global Event TournamentSelected
   * @param tournament - Tournament data returned from the server after successful creation.
   * @private
   */
  private _handleSuccessfulImportEvent(tournament: ITournament) {
    this.submitButtonColor = 'light';
    this.tournamentForm.disable();
    this._presentImportSuccessToast().then(
      () => {
        this._tournamentService.setTournament(tournament)
          .then(() => {
            this._sidebarService.refreshSidebar();
            this._navCtrl.pop()
          })
      }
    )
  }
  
  /**
   * After POST success, display an alert of success.
   * @private
   */
  private _presentImportSuccessToast(): Promise<any> {
    return this._toastCtrl.create({
      message: 'Tournament import was successful.',
      duration: 2000,
      position: 'top'
    }).present()
  }
  
  /**
   * After POST fail, display an alert of failure.
   * @private
   */
  private _presentImportFailureAlert() {
    this._alertCtrl.create({
      title: 'Whoops!',
      subTitle: 'Something went wrong on Tournament import.',
      buttons: ['Dismiss']
    }).present()
  }
  
  /**
   * Present the failure alert if we fail on tournament creation.
   * @private
   */
  private _presentRequiredFailedAlert() {
    let alert = this._alertCtrl.create({
      title: 'Required Fields!',
      subTitle: 'Fill out all required fields before submitting.',
      buttons: ['Dismiss']
    });
    alert.present();
  }
  
  /**
   * Create a new loader and present it.
   * @param {LoadingOptions} opts - options for loader
   * @return {Loading}
   * @private
   */
  private _getAndPresentLoader(opts?: LoadingOptions): Loading {
    let loader = this._loaderCtrl.create(opts);
    loader.present();
    return loader;
  }
  
  //#endregion
  
  //#endregion
  
  
}
