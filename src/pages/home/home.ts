import {Component, ViewChild} from '@angular/core';
import {FabContainer, NavController} from 'ionic-angular';
import {GlobalEventService, GlobalEventTopics} from '../../providers/global-event-service';
import {ITournament} from '../../../models/models';
import {UserService} from '../../providers/user-service';
import {ImportPage} from '../import/import';
import {SidebarService} from '../../providers/sidebar-service';
import {TestPage} from '../test/test';
import {ENV} from '@app/env';
import { TournamentService } from '../../providers/tournament-service';
import { EditComponent } from './components/edit.component';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  //view children
  @ViewChild("menuFab") public menuFab: FabContainer;
  @ViewChild(EditComponent) private _editComponent: EditComponent;
  
  
  public tournament: ITournament;
  
  constructor(private _globalEventService: GlobalEventService,
              private _tournamentService: TournamentService,
              private _userService: UserService,
              private _sidebarService: SidebarService,
              private _navCtrl: NavController) {
    //subscribe to TournamentSelected event.
    this._tournamentService.subscribeTournamentSelected(this._tournamentSelectedEventHandler.bind(this))
  }
  
  ionViewDidEnter() {
    //show sidebar when home page enters
    this._sidebarService.showSidebar();
  }
  
  /**
   * Log out
   */
  public logout() {
    this.menuFab.close();
    this._globalEventService.publish(GlobalEventTopics.UserLogout);
  }
  
  /**
   * Open tournament import page.
   */
  public openImportPage() {
    this.menuFab.close();
    this._navCtrl.push(ImportPage);
  }
  
  /**
   * Open test view page
   */
  public testView() {
    this.menuFab.close();
    this._navCtrl.push(TestPage)
  }
  
  /**
   * Dev environment?
   * @returns {boolean}
   */
  public isTestEnv(): boolean {
    return ENV.mode === 'dev';
  }
  
  /**
   * Is tournament loading?
   * @returns {boolean}
   */
  public get isLoading(): boolean {
    return this._editComponent.isLoading;
  }
  
  /**
   * Set tournament member
   * @param {ITournament} tournament
   * @private
   */
  private _tournamentSelectedEventHandler(tournament: ITournament) {
    this.tournament = tournament;
  }
  
}
