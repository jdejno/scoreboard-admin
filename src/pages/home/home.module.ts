import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {HomePage} from './home';
import {PipeModule} from '../../pipes/pipe.module';
import {EditComponent} from './components/edit.component';
import {TournamentForm} from './components/forms/tournament.form';
import {HoverTextDirective} from '../../directives/hover-text.directive';
import {PoolForm} from './components/forms/pool.form';
import {PoolModalComponent} from './components/modals/pool.modal';
import {ComponentsModule} from '../../components/components.module';
import {TeamForm} from './components/forms/team.form';
import {DivisionForm} from './components/forms/division.form';

@NgModule({
  declarations: [
    HomePage,
    EditComponent,
    TournamentForm,
    DivisionForm,
    HoverTextDirective,
    PoolForm,
    PoolModalComponent,
    TeamForm
  ],
  imports: [
    PipeModule,
    ComponentsModule,
    IonicPageModule.forChild(HomePage),
  ],
  entryComponents: [
    EditComponent,
    TournamentForm,
    DivisionForm,
    PoolForm,
    TeamForm,
    PoolModalComponent
  ]
})
export class HomePageModule {}
