import { Component } from '@angular/core';
import { ITournament } from '../../../../models/models';
import { DashboardPage } from '../../dashboard/dashboard';
import { NavController } from 'ionic-angular';
import { TournamentService } from '../../../providers/tournament-service';
import { DataService } from '../../../providers/data-service';

@Component({
  selector: 'home-edit',
  templateUrl: 'edit.component.html',
})
export class EditComponent {
  
  public tournament: ITournament;
  public isLoading: boolean;
  
  constructor(private _navCtrl: NavController,
              private _dataService: DataService,
              private _tournamentService: TournamentService) {
    this._tournamentService.getTournament().filter(tournament => !!tournament).subscribe(this._getTournamentAllData.bind(this));
    this._tournamentService.subscribeTournamentSelected(this._getTournamentAllData.bind(this));
  }
  
  /**
   * Open the Dashboard for this tournament.
   */
  public openDashboard() {
    this._navCtrl.push(DashboardPage, {})
  }
  
  /**
   * Handle division deletion.
   * @param {string} divisionId
   */
  public onDeleteDivision(divisionId: string) {
    this._dataService.removeDivisionFromTournament(this.tournament._id, divisionId).subscribe(tournament => {
      this.tournament.divisions = this.tournament.divisions.filter(division => division._id !== divisionId)
    })
  }
  
  /**
   * Get tournament with all data and models populated.
   * @param {ITournament} tournament
   * @private
   */
  private _getTournamentAllData(tournament: ITournament) {
    this.isLoading = true;
    this._dataService.getTournamentAllData(tournament._id).subscribe(tournament => {
      this.tournament = tournament;
      this.isLoading = false;
    })
  }
  
  
}
