import {Component, ViewChild} from '@angular/core';
import {ViewController} from 'ionic-angular';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TournamentService} from '../../../../providers/tournament-service';
import {ITournament} from '../../../../../models/models';
import _ from 'lodash';

@Component({
  selector: 'pool-modal',
  templateUrl: 'pool.modal.html'
})
export class PoolModalComponent {
  
  public form: FormGroup;
  
  @ViewChild('titleInput')
  public titleInput;
  
  constructor(private _viewController: ViewController,
              private _tournamentService: TournamentService,
              private fb: FormBuilder) {
    this._tournamentService.getTournament().subscribe(tournament => this.form = this._createPoolForm(tournament));
  }
  
  ionViewDidEnter() {
    //without the small timeout, something is taking away the focus from this.
    setTimeout(() => {this.titleInput.setFocus();}, 50)
    
  }
  
  /**
   * Create the form for pool
   * @param {ITournament} tournament
   * @private
   */
  private _createPoolForm(tournament: ITournament): FormGroup {
    return this.fb.group({
      title: ['', Validators.required],
      startTime: [_.first(tournament.dates), Validators.required],
      startDate: [_.first(tournament.dates), Validators.required],
      location: this.fb.group({
        location: '',
        court: ''
      }),
      format: this.fb.group({
        gameScores: this.fb.array([])
      })
    })
  }
  
  /**
   * Update the score FormArray within the format FormGroup
   * @param {FormArray} scoresFormArray
   */
  public updateScoreFormArray(scoresFormArray: FormArray) {
    let formats = this.form.get('format') as FormGroup;
    formats.setControl('gameScores', scoresFormArray);
  }
  
  /**
   * Dismiss the modal with the pool data if the form is valid.
   */
  public submit() {
    if (this.form.valid) {
      this.dismiss(this.form);
    }
  }
  
  /**
   * Dismiss and pass back pool data to consuming code
   * @note: This will only be available when the button is enabled (aka. form is valid)
   */
  public dismiss(form: FormGroup) {
    this._viewController.dismiss(form.getRawValue());
  }
  
  /**
   * Dismiss the form without passing back the pool data.
   */
  public cancel() {
    this._viewController.dismiss();
  }
  
}
