import { Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { ITournament } from '../../../../../models/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../../../../providers/data-service';
import { Subject } from 'rxjs/Subject';
import { SidebarService } from '../../../../providers/sidebar-service';
import { STATES } from '../../../../providers/value-providers/states';
import { TournamentService } from '../../../../providers/tournament-service';
import _ from 'lodash';
import { FormService } from '../../../../providers/form.service';


@Component({
  selector: 'home-edit-tournament-form',
  templateUrl: 'tournament.form.html',
})
export class TournamentForm {
  
  @Input() set tournament(tournament: ITournament) {
    this._tournament = tournament;
    this._initializeTournamentForm(tournament);
  }
  
  get tournament() { return this._tournament }
  
  /**
   * Let parent component we want to be deleted.
   */
  
  //public members
  public tournamentForm: FormGroup;
  
  //private members
  private _tournament: ITournament;
  private _onDestroy$: Subject<void>;
  
  
  constructor(private _formService: FormService,
              private _dataService: DataService,
              private _sidebarService: SidebarService,
              private _tournamentService: TournamentService,
              @Inject(STATES) public states: Array<string>) {
    this._onDestroy$ = new Subject();
  }
  
  // TODO: ngOnDestroy doesn't do anything here, since this is embedded in a parent component
  ngOnDestroy() {
    //always update upon leaving
    this.updateTournament();
    this._onDestroy$.next();
    this._onDestroy$.complete();
  }
  
  /**
   * (Re)Initialize tournament form
   * @param {ITournament} tournament
   * @private
   */
  private _initializeTournamentForm(tournament: ITournament): void {
    this.tournamentForm = this._formService.createTournamentForm(tournament);
    this._subscribeValueChanges(this.tournamentForm);
  }
  
  //#region public members
  
  /**
   * Update tournament on the server.
   * If failed, present a failure alert.
   * @param {FormGroup} tournamentForm
   */
  public updateTournament() {
    if (!this._formService.formNeedsUpdating(this._tournament._id, this.tournamentForm)) return;
    
    this._dataService.updateTournament(this._tournament._id, this._transformTournamentFormRawValue(this.tournamentForm))
      .subscribe(
        tournament => {
          this._updateSidebar(tournament);
          this._initializeTournamentForm(tournament);
          this._tournamentService.updateTournament(tournament);
        }, () => {
          this._presentUpdateFailure();
        }
      )
  }
  
  //#endregion
  
  //#region private methods
  
  /**
   * Handle to value changes made to tournamentForm
   * @param {FormGroup} tournamentForm
   * @private
   */
  private _subscribeValueChanges(tournamentForm: FormGroup): void {
    tournamentForm.valueChanges
      .debounceTime(1000)
      .takeUntil(this._onDestroy$)
      .subscribe(() => this.updateTournament())
  }
  
  /**
   *
   * @param {FormGroup} tournamentForm
   * @return {ITournament}
   * @private
   */
  private _transformTournamentFormRawValue(tournamentForm: FormGroup): ITournament {
    let tournament = tournamentForm.getRawValue();
    tournament.dates = [tournament.dates.start, tournament.dates.end];
    tournament.status = tournament.status ? 'ACTIVE' : 'DRAFT';
    return tournament;
  }
  
  private _presentUpdateFailure(): void {
  
  }
  
  /**
   * Refresh the sidebar if the relevant data has been changed.
   * @param {ITournament} tournament
   * @private
   */
  private _updateSidebar(tournament: ITournament): void {
    if (this._tournament.title !== tournament.title) return this._sidebarService.refreshSidebar();
    if (!_.isEqual(this._tournament.dates, tournament.dates)) return this._sidebarService.refreshSidebar();
  }
  
  
  //#endregion
  
}
