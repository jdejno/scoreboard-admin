import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ITeam} from '../../../../../models/models';
import {DataService} from '../../../../providers/data-service';
import _ from 'lodash';
import { FormService } from '../../../../providers/form.service';


@Component({
  selector: 'team-form',
  templateUrl: 'team.form.html'
})
export class TeamForm {
  
  //private members
  private _team: ITeam;
  
  //public members
  public form: FormGroup;
  public isFormUpdating: boolean;
  
  //#region inputs
  
  @Input()
  public set team(team: ITeam) {
    this._team = team;
    this.form = this._formService.createTeamForm(team);
  }
  public get team() {return this._team}
  
  @Input()
  orderNum: number;
  
  //#endregion
  
  //#region outputs
  
  //let parent component know this team should be destroyed
  @Output()
  destroyTeam: EventEmitter<string> = new EventEmitter<string>();
  
  //let parent component know this team is currently updating.
  @Output()
  formIsUpdating: EventEmitter<boolean> = new EventEmitter<boolean>();
  
  //let parent component know the team has been updated.
  @Output()
  teamUpdated: EventEmitter<ITeam> = new EventEmitter<ITeam>();
  
  //#endregion
  
  constructor(private _dataService: DataService,
              private _formService: FormService) {}
  
  /**
   * Update the team's title on the DB.
   */
  public updateTeamTitle() {
    if (!this._formService.formNeedsUpdating(this._team._id, this.form)) return;
    
    this._setFormIsUpdating(true);
    this._dataService.changeTeamName(this._team._id, this.form.get('title').value)
      .subscribe((team: ITeam) => {
        this.teamUpdated.emit(team);
        this._setFormIsUpdating(false);
        this.team = team;
      });
  }
  
  /**
   * Remove and destroy team. Notify parent component.
   */
  public removeTeam() {
    this.destroyTeam.emit(this._team._id);
  }
  
  /**
   * Whether form needs updating or not.
   * @returns {boolean}
   */
  public formNeedsUpdating(): boolean {
    return this._formService.formNeedsUpdating(this._team._id, this.form);
  }
  
  /**
   * Set is form is updating and notify
   * @param {boolean} isUpdating
   * @private
   */
  private _setFormIsUpdating(isUpdating: boolean) {
    this.isFormUpdating = isUpdating;
    this.formIsUpdating.emit(isUpdating);
  }
  
}
