import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataService } from '../../../../providers/data-service';
import { IDivision, IPool } from '../../../../../models/models';
import { FormGroup } from '@angular/forms';
import { FormService } from '../../../../providers/form.service';
import { PoolModalComponent } from '../modals/pool.modal';
import { AlertController, ModalController } from 'ionic-angular';
import _ from 'lodash';


@Component({
  selector: 'division-form',
  templateUrl: 'division.form.html'
})
export class DivisionForm {
  
  //private members
  private _division: IDivision;
  
  //public members
  public form: FormGroup;
  
  /**
   * Set division and create FormGroup.
   * @param {IDivision} division
   */
  @Input()
  public set division(division: IDivision) {
    this._division = division;
    this.form = this._formService.createDivisionFrom(division);
    this._handleFormValueChanges(this.form);
  }
  
  public get division() {return this._division}
  
  /**
   * Let parent know this division should be deleted.
   * @type {EventEmitter<string>}
   */
  @Output() deleteDivision: EventEmitter<string> = new EventEmitter<string>();
  
  constructor(private _dataService: DataService,
              private _modalCtrl: ModalController,
              private _alertCtrl: AlertController,
              private _formService: FormService) {}
  
  //#region UI event handlers
  
  public removeDivision() {
    this._presentRemoveDivisionPrompt();
  }
  
  /**
   * Handle add pool click event.
   * Presents a pool modal form.
   */
  public addPool() {
    this._presentAddPoolPrompt();
  }
  
  /**
   * Remove a pool from the division
   * @param {string} poolId - id of pool to remove.
   */
  public removePool(poolId: string) {
    this._dataService.removePoolFromDivision(this._division._id, poolId)
      .subscribe(() => this._division.pools = _.remove(this._division.pools, pool => pool._id !== poolId));
  }
  
  //#endregion
  
  //#region private methods
  
  //#region UI handlers
  
  private _presentRemoveDivisionPrompt() {
    let alert = this._alertCtrl.create({
      title: `Delete Division`,
      subTitle: `Are you sure you want to delete ${this._division.gender} ${this._division.level} Division?`,
      buttons: [
        {
          text: 'Yes, Delete',
          handler: () => this.deleteDivision.emit(this._division._id)
        },
        {
          text: 'No, cancel'
        }
      ]
    });
    alert.present();
  }
  
  /**
   * Present user with pool creation modal.
   * @private
   */
  private _presentAddPoolPrompt() {
    let modal = this._modalCtrl.create(PoolModalComponent);
    modal.onDidDismiss(pool => {
      if (pool) {
        this._addPool(pool)
      }
    });
    modal.present();
  }
  
  /**
   * Add pool to division on DB.
   * @param {IPool} pool
   * @private
   */
  private _addPool(pool: IPool) {
    this._dataService.addNewPoolToDivision(this._division._id, pool)
      .subscribe(pool => this._division.pools.push(pool))
  }
  
  //#endregion
  
  /**
   * Handle value changes made to the division form.
   * @note
   *  If you are adding editable field to the division metadata,
   *  you must add that property to the properties updated in the
   *  cached division object from the serve response.
   *
   * @param {FormGroup} form
   * @private
   */
  private _handleFormValueChanges(form: FormGroup) {
    form.valueChanges.debounceTime(1000).subscribe((division: IDivision) => {
      this._dataService.updateDivision(this._division._id, division)
        .subscribe(division => {
          this._division.level = division.level;
          this._division.gender = division.gender;
        });
    })
  }
  
  //#endregion
  
}
