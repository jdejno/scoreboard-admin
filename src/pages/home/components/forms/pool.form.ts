import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Alert, AlertController, reorderArray } from 'ionic-angular';
import { IPool, ITeam } from '../../../../../models/models';
import { DataService } from '../../../../providers/data-service';
import { TournamentService } from '../../../../providers/tournament-service';
import { Observable } from 'rxjs/Observable';
import _ from '../../../../lib/myLodashLibrary';
import moment from 'moment';
import { FormService } from '../../../../providers/form.service';

@Component({
  selector: 'pool-form',
  templateUrl: 'pool.form.html'
})
export class PoolForm {
  
  //private members
  private _pool: IPool;
  
  //public members
  public form: FormGroup;
  public isFormUpdating: boolean;
  
  /**
   * Set IPool for component. Creates form and caches pool.
   * @param {IPool} pool
   */
  @Input('pool')
  public set pool(pool: IPool) {
    this._pool = pool;
    this._initializePoolForm(pool);
  }
  
  public get pool() {return this._pool}
  
  /**
   * Notify parent component that pool should be destroyed.
   * @type {EventEmitter<string>} - poolId
   */
  @Output('destroyPool')
  removePoolId: EventEmitter<string> = new EventEmitter<string>();
  
  
  constructor(private _alertController: AlertController,
              private _formService: FormService,
              private _tournamentService: TournamentService,
              private _dataService: DataService) {}
  
  //#region public methods
  
  /**
   * Reorder the teams within the view and on the DB.
   * @param indexes - indexes to use for reordering the team within the pool
   */
  public reorderTeams(indexes) {
    //TODO: Need to fix the reordering here.
    this.isFormUpdating = true;
    let teams = reorderArray(this._pool.teams, indexes);
    this._dataService.reorderTeamInPool(this._pool._id, _.map(teams, '_id'))
      .subscribe(pool => {
        this._pool = pool;
        this.isFormUpdating = false;
      })
  }
  
  /**
   * Add a team to the pool. Create and present the alert prompting the user for the team array.
   * Callback to the database to add the new team to the pool is handled by the Alert that is presented.
   */
  public addTeam(): void {
    this._getAddTeamPrompt().present();
  }
  
  /**
   * Remove a team from a pool.
   * Make a data request to the sever to update the pool and division model by removing the team.
   * @param {string} teamId - id of team to remove from pool.
   */
  public removeTeam(teamId: string) {
    this.isFormUpdating = true;
    this._dataService.removeTeamFromPool(this._pool._id, teamId)
      .subscribe(pool => {
        this.pool = pool;
        this.isFormUpdating = false;
      })
  }
  
  /**
   * Update the pool on the server.
   * @param {IPool} pool
   */
  public updatePool(pool: IPool) {
    if (!this._formService.formNeedsUpdating(this._pool._id, this.form)) return;
    
    this.isFormUpdating = true;
    this._dataService.updatePool(this._pool._id, pool)
      .subscribe((pool: IPool) => {
        this.pool = pool;
        this.isFormUpdating = false;
      })
  }
  
  /**
   * Handle when the team form is updating.
   * @param {boolean} isUpdating
   */
  public handleTeamFormUpdating(isUpdating: boolean) {
    this.isFormUpdating = isUpdating;
  }
  
  /**
   * Handle when team form changes.
   * @param {ITeam} team
   */
  public teamFormUpdated(team: ITeam) {
    // this._patchTeamNameChange(team);
  }
  
  /**
   * Output to let parent know to remove this pool.
   */
  public destroyPool() {
    this.removePoolId.emit(this._pool._id);
  }
  
  //#endregion
  
  
  //#region private methods
  
  /**
   * Create form from pool and subscribe to any form changes.
   * @param {IPool} pool
   * @private
   */
  private _initializePoolForm(pool: IPool) {
    this.form = this._formService.createPoolForm(pool);
    this._handlePoolFormChanges(this.form);
  }
  
  /**
   * Handle changes to data in the FormGroups of IPool data.
   * @param {FormGroup} poolForm - FormGroup of a single IPool datum.
   * @private
   */
  private _handlePoolFormChanges(poolForm: FormGroup) {
    poolForm.valueChanges
      .debounceTime(1000)
      .subscribe((pool: IPool) => this.updatePool(this._transformPoolTimes(pool)));
  }
  
  
  //#region private UI handlers
  
  /**
   * Create and return the Alert prompt for adding a new team to a pool.
   * @return {Alert} - Alert that is to be presented to the end user for the team title input
   * @private
   */
  private _getAddTeamPrompt(): Alert {
    return this._alertController.create({
      title: 'Add Team',
      inputs: [
        {
          name: 'title',
          placeholder: 'Team name'
        }
      ],
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Add',
          handler: data => {
            if (data.title) {
              this._addTeam(data.title);
              return true
            }
            return false
          }
        }
      ]
    });
  }
  
  /**
   * Database call to update the pool and division with the new added team.
   * @param {FormGroup} poolForm - poolForm that is to be updated.
   * @param {string} title - title of the team.
   * @private
   */
  private _addTeam(title: string) {
    this.isFormUpdating = true;
    this._tournamentService.getTournament().subscribe(
      tournament =>
        tournament
          ? this._dataService.addTeamToPool(this._pool._id, {
            teamTitle: title,
            tournamentId: tournament._id
          }).subscribe((pool: IPool) => {
            this._pool = pool;
            this._initializePoolForm(pool);
            this.isFormUpdating = false;
          })
          : Observable.throw(new Error('No Tournament selected during add team to pool.'))
    )
  }
  
  
  //#endregion
  
  
  private _transformPoolTimes(pool: IPool): IPool {
    if (pool.startTime) {
      pool.startTime = moment(pool.startTime).toDate();
    }
    return pool;
  }
  
  
  //#endregion
  
  
}

