import {AbstractControl, FormArray, FormControl, FormGroup} from '@angular/forms';
import {Injectable} from '@angular/core';
import {IGameFormat} from './game-format-form';
import _map from 'lodash/map';
import _range from 'lodash/range';
import _cloneDeep from 'lodash/cloneDeep';

/**
 * Contains business logic for creating, adding, and removing FormControls to/from a FormArray.
 * Tries to intelligently add last game format and inserting game formats correctly to FormArray.
 */
@Injectable()
export class GameFormatService {
  
  private _cachedLastGameFormatGroup: AbstractControl;
  
  constructor() {}
  
  /**
   * Converts an Array of IGameFormat objects to a new FormArray.
   * @param {Array<IGameFormat>} gameFormats - Array of game format objects to convert to FormArray
   * @returns {FormArray} - new FormArray of with IGameFormat data.
   */
  toFormArray(gameFormats: Array<IGameFormat>): FormArray {
    return new FormArray(gameFormats.map(format => this._toFormGroup(format)));
  }
  
  /**
   * Gets the default FormArray if we do not have user specified data yet..
   * @param {number} numOfGames - number of games to add FormGroups for.
   * @returns {FormArray} - new FormArray
   */
  public getDefaultFormArray(numOfGames: number) {
    let gameFormats = _map(_range(numOfGames), game => {
      return game < numOfGames - 1 ? {target: 25, cap: 27} : {target: 15, cap: Infinity};
    });
    return this.toFormArray(gameFormats);
  }
  
  /**
   * Update the FormArray based on numOfGames passed in.
   * @desc add, remove (or create default) FormGroups from FormArray.
   * @param {FormArray} formArray - FormArray to update.
   * @param {number} numOfGames - number of games to gauge adding/removing FormGroups from FormArray
   * @returns {FormArray}
   */
  updateFormArray(formArray: FormArray, numOfGames: number): FormArray {
    //cache the last game format for use later.
    this._cachedLastGameFormatGroup = this._cacheLastGameFormat(formArray, numOfGames);
    
    if (formArray.length > numOfGames) {
      return this._removeFormGroups(formArray);
    } else {
      return this._addFormGroups(formArray);
    }
    
  }
  
  /**
   * Transform a IGameFormat object to a FormGroup.
   * @param {IGameFormat} format - game format object to transform.
   * @returns {FormGroup} - new FormGroup
   * @private
   */
  private _toFormGroup(format: IGameFormat): FormGroup {
    return new FormGroup({
      target: new FormControl(format.target),
      cap: new FormControl(format.cap)
    })
  }
  
  /**
   * Remove a FormGroup from the FormArray.
   * @desc Handles removing the second last vs. greater situations different.
   * @param {FormArray} formArray - FormArray to remove FormGroup from.
   * @returns {FormArray} - modified FormArray
   * @private
   */
  private _removeFormGroups(formArray: FormArray): FormArray {
    if(formArray.length < 3) {
      formArray.removeAt(formArray.length - 1);
    } else {
      formArray.removeAt(formArray.length - 2);
    }
    return formArray;
  }
  
  /**
   * Add a FormGroup to the FormArray
   * Using either _cachedLastGameFormat or copying from second last item in FormArray.
   * @desc Handles adding the second vs. greater situations different. (check the code)
   * @param {FormArray} formArray - FormArray to add FormGroup from.
   * @returns {FormArray} - modified FormArray
   * @private
   */
  private _addFormGroups(formArray: FormArray): FormArray {
    if(formArray.length < 2) {
      formArray.push(_cloneDeep(this._cachedLastGameFormatGroup));
    } else {
      formArray.insert(formArray.length - 2, _cloneDeep(formArray.at(formArray.length - 2)));
    }
    return formArray;
  }
  
  /**
   * Get the correct formGroup to cache for _cacheLastGameFormat.
   * @param {FormArray} formArray - FormArray of IGameFormats
   * @param {number} numOfGames - number of games for match.
   * @return {AbstractControl} - FormGroup of IGameFormat.
   * @private
   */
  private _cacheLastGameFormat(formArray: FormArray, numOfGames: number): AbstractControl {
    return (numOfGames > 1 && formArray && formArray.length > 2) //if numOfGames greater than 1 and formArray has 3 items.
      ? formArray.at(formArray.length - 1)                //return last item of formArray.
      : this._cachedLastGameFormatGroup;                        //else, return previously cached value.
  }
  
}
