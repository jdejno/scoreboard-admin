import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import _range from 'lodash/range';
import { FormArray } from '@angular/forms';
import { GameFormatService } from './game-format-control';

export interface IGameFormat {
  target: number
  cap: number
}

@Component({
  selector: 'game-format-form',
  templateUrl: 'game-format-form.html'
})
export class GameFormatFormComponent implements OnInit {
  
  //template variables
  public gameScoreOptions: Array<number>;
  public form: FormArray;
  
  //private
  private _numOfGames: number;
  
  @Input()
  public set numOfGames(num: number) {
    this._numOfGames = num;
    this.form = !this.form
      ? this._gameFormatService.getDefaultFormArray(this._numOfGames)
      : this._gameFormatService.updateFormArray(this.form, this._numOfGames);
    this.scoresFormArray.emit(this.form);
  }
  
  public get numOfGames() {return this._numOfGames}
  
  @Output()
  public scoresFormArray: EventEmitter<FormArray> = new EventEmitter();
  
  
  constructor(private _gameFormatService: GameFormatService) {
    this.gameScoreOptions = _range(1, 32).concat(Infinity);
  }
  
  ngOnInit() {
    if(!this._numOfGames) {
      this.numOfGames = 3;
    }
  }
  
  
}
