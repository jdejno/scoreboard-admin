import {ComponentFactory, ComponentFactoryResolver, ComponentRef, Directive, ViewContainerRef} from '@angular/core';
import {ISidebarComponent, SidebarService, SidebarTypes} from '../../providers/sidebar-service';
import {HomeSidebarComponent} from './home-sidebar.component';


@Directive({
  selector: 'sidebar-manager'
})
export class SidebarComponentManager {
  
  
  private _sidebarFactoryMap: Map<SidebarTypes, ComponentFactory<ISidebarComponent>>;
  private _currSidebarComponentRef: ComponentRef<ISidebarComponent>;
  
  constructor(private _sidebarService: SidebarService,
              private _sidebarContainer: ViewContainerRef,
              private _componentFactory: ComponentFactoryResolver) {
    
    this._initializeSidebarFactoryMap();
    //subscribe to sidebar sets and refreshes
    this._sidebarService.getSetSidebarSubject().subscribe(this._renderNewSidebar.bind(this));
    this._sidebarService.getRefreshSidebarSubject().subscribe(this._refreshSidebar.bind(this));
    this._sidebarService.getDestroySidebarSubject().subscribe(this._destroySidebar.bind(this))
    
  }
  
  
  /**
   * Clear all current ISidebarComponents and set new sidebar.
   * @param {SidebarTypes} type - type of sidebar to render
   * @private
   */
  private _renderNewSidebar(type: SidebarTypes) {
    this._sidebarService.hideSidebar();
    this._sidebarContainer.clear();
    this._currSidebarComponentRef = this._sidebarContainer.createComponent<ISidebarComponent>(this._sidebarFactoryMap.get(type));
    this._currSidebarComponentRef.instance.initialize();
    this._sidebarService.showSidebar();
  }
  
  /**
   * Refresh all current sidebars.
   * @private
   */
  private _refreshSidebar() {
    if (this._currSidebarComponentRef) {
      this._currSidebarComponentRef.instance.refresh();
    }
  }
  
  /**
   * Destroy the current sidebar component.
   * @private
   */
  private _destroySidebar() {
    if (this._currSidebarComponentRef) {
      this._currSidebarComponentRef.destroy();
    }
  }
  
  /**
   * Create map of sidebar components to enum of SidebarTypes
   * @private
   */
  private _initializeSidebarFactoryMap(): void {
    this._sidebarFactoryMap = new Map<SidebarTypes, ComponentFactory<ISidebarComponent>>([
      [SidebarTypes.TournamentSelection, this._componentFactory.resolveComponentFactory<ISidebarComponent>(HomeSidebarComponent)]
    ]);
    
  }
  
}



