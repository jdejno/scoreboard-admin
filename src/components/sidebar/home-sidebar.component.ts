import {Component} from '@angular/core';
import {IUser} from '../../models/models';
import {Observable} from 'rxjs/Observable';
import {ITournament} from '../../../models/models';
import {DataService} from '../../providers/data-service';
import {UserService} from '../../providers/user-service';
import {ISidebarComponent} from '../../providers/sidebar-service';
import {TournamentService} from '../../providers/tournament-service';


@Component({
  selector: 'home-sidebar',
  templateUrl: 'home-sidebar.component.html'
})
export class HomeSidebarComponent implements ISidebarComponent {
  
  public selectedTournamentId: string;
  public user: IUser;
  public $tournaments: Observable<Array<ITournament>>;
  public isInitiated: boolean;
  
  constructor(private _dataService: DataService,
              private _userService: UserService,
              private _tournamentService: TournamentService) {
    //update active button on tournament selected.
    this._tournamentService.subscribeTournamentSelected(tournament => this.selectedTournamentId = tournament._id);
  }
  
  /**
   * Initialize the data of the sidebar component.
   * This can be called by a parent component for re-instantiating the user data if it has been
   * updated since initial render.
   */
  public initialize(): void {
    //assign observable emitting array of tournaments to our public this.$tournaments for use in template.
    this.$tournaments = this._userService.getUser()
      .do(user => this.user = user)
      .switchMap((user) => {
        return user ?
          this._dataService.getTournamentsByCreator(user._id)
            .do(() => this.isInitiated = true) :
          Observable.empty();
      })
    
  }
  
  public refresh(): void {
    this.initialize();
  }
  
  /**
   * Publish the tournament selection to the global event service.
   * @param {ITournament} tournament - tournament selected.
   */
  public onTournamentSelect(tournament: ITournament) {
    this._tournamentService.getTournament().subscribe(
      storedTournament => {
        //only set tournament if different from already selected.
        if (!storedTournament || storedTournament._id !== tournament._id) {
          this._tournamentService.setTournament(tournament);
        }
      }
    )
    
  }
  
}
