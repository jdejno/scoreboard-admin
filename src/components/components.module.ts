import {NgModule} from '@angular/core';
import {GameFormatFormComponent} from './game-format-form/game-format-form';
import {IonicModule} from 'ionic-angular';
import {PipeModule} from '../pipes/pipe.module';
import { GameFormatService } from './game-format-form/game-format-control';

@NgModule({
  declarations: [
    GameFormatFormComponent,
  ],
  imports: [
    IonicModule,
    PipeModule
  ],
  exports: [
    GameFormatFormComponent,
  ],
  providers: [GameFormatService]
})
export class ComponentsModule {
}
