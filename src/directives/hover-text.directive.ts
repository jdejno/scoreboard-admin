import {AfterViewInit, Directive, ElementRef, HostListener, Inject, Input, Renderer2} from '@angular/core';
import {DOCUMENT} from '@angular/common';

//TODO: calculate width of html text automatically

export interface IHoverTextOptions {
  text: string
  direction: 'up' | 'down' | 'right' | 'left'
  color: string
  width: number
}

@Directive({
  selector: '[hoverTextDisplay]'
})
export class HoverTextDirective implements AfterViewInit {
  
  @Input() hoverTextOptions: IHoverTextOptions;
  @Input() hoverTextColor: string;
  
  private _pseudoStyle: HTMLStyleElement;
  private _head: HTMLHeadElement;
  private _hoverDiv: HTMLDivElement;
  
  constructor(private el: ElementRef,
              @Inject(DOCUMENT) private document: any,
              private renderer: Renderer2) {
    this._initiateHTMLElements();
  }
  
  @HostListener('mouseenter') onHover(event) {
    this.renderer.addClass(this._hoverDiv, 'hover-text-active')
  }
  
  @HostListener('mouseleave') onLeave() {
    this.renderer.removeClass(this._hoverDiv, 'hover-text-active')
  }
  
  ngAfterViewInit() {
    //set defaults
    this.hoverTextOptions = {
      text: this.hoverTextOptions.text || '',
      direction: this.hoverTextOptions.direction || 'left',
      color: this.hoverTextOptions.color || 'black',
      width: this.hoverTextOptions.width || 180
    };
    //add the div and pseudo element
    this._addHoverElement()
  }
  
  /**
   * Add the div to this.el (as parent) and apply pseudo element styling.
   * @private
   */
  private _addHoverElement() {
    //get unique id for text-hover-id
    let id = (this.renderer.data['text-hover-id'] || 0) + 1;
    this.renderer.data['text-hover-id'] = id;
    
    //create a new div to apply our pseudo style class to.
    this._hoverDiv = this.renderer.createElement('div');
    this.renderer.setAttribute(this._hoverDiv, 'id', 'div-text-hover-' + id);
    this.renderer.appendChild(this.el.nativeElement, this._hoverDiv);
    
    //add the appropriate class to the parent element
    this.renderer.addClass(this.el.nativeElement, 'hover-text-container');
    
    //add the styles to the hover div and pseudo element
    this._addHoverElementStyles();
    
  }
  
  /**
   * Add the styles to our hover-pseudo-styles section for this particular id.
   * @private
   */
  private _addHoverElementStyles() {
    //div styles
    this._pseudoStyle.innerHTML += `#${this._hoverDiv.id} {
      position: absolute;
      overflow: hidden;
      height: inherit;
      top: -5px;
      left: -${this.hoverTextOptions.width}px;
    } `;
    
    //pseudo styles (including text content and color)
    this._pseudoStyle.innerHTML += `#${this._hoverDiv.id}::before {
      content: "${this.hoverTextOptions.text}";
      color: ${this.hoverTextOptions.color};
      transition: all .5s;
      display: inline-block;
      height: inherit;
      position: relative;
      top: 0;
      left: ${this.hoverTextOptions.width}px;
     } `;
    
  }
  
  /**
   * Get or create the references to our style HTMLStyleElement and HTMLHeadElement
   * @private
   */
  private _initiateHTMLElements() {
    this._head = this.document.head || this.document.getElementsByTagName('head')[0];
    if (this._pseudoStyle = this.document.getElementById('hover-pseudo-styles')) return;
    
    this._pseudoStyle = this.renderer.createElement('style');
    /**
     *    <style id="hover-pseudo-styles" type="text/css">
     *      .hover-text-container {
     *        overflow: hidden
     *      }
     *    </style>
     */
    this._pseudoStyle.innerHTML += '.hover-text-container { overflow: visible; contain: size; } ';
    this._pseudoStyle.innerHTML += '.hover-text-active::before { transition: all .5s !important; left: 0 !important; } ';
    this.renderer.setAttribute(this._pseudoStyle, 'type', 'text/css');
    this.renderer.setAttribute(this._pseudoStyle, 'id', 'hover-pseudo-styles');
    this.renderer.appendChild(this._head, this._pseudoStyle);
    
  }
  
  
}
