export const ENV: any = {
  mode: 'dev',
  api: 'http://localhost:3000',
  facebook: {
    appid: '1929803343731095',
    permissions: ['public_profile', 'user_friends', 'email']
  },
  socketio: {
    url: 'http://localhost:3000',
    options: {}
  }
};
