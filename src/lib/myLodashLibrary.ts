import _ from 'lodash';

_.mixin({'omitNilDeep': omitNilDeep});
export default _;

/********************************************
 * Dates
 ********************************************/


/********************************************
 * Objects
 *******************************************/
/**
 * Remove all null primitive values and empty object/arrays from parent array or object.
 * @param {T} o - object/array to remove nils from
 * @returns {T} - pruned object/array
 * @private
 */
function omitNilDeep<T extends Object | Array<any>>(o: T): T {
  if (_.isArray(o)) {
    return _(o).map(_omitNilIterate.bind(this)).compact().value();
  }
  if (_.isPlainObject(o)) {
    return _(o).mapValues(_omitNilIterate.bind(this)).omitBy(_.isNil).value();
  }
  return o;
  
  function _omitNilIterate<T>(o: any): T {
    o = _.isPlainObject(o) ? _.omitBy(o, _.isNil) : o;
    o = _.isEmpty(o) ? null : o;
    return this.omitNilDeep(o);
  }
}








