import _ from 'lodash';
import {IGame} from '../../models/models';

/**
 * GET game winner based on score
 * @param {IGame} game
 * @return {string}
 */
export function getGameWinner(game: IGame): string {
  if (!gameIsFinished(game))
    return;

  let finalScore = getGameFinalScore(game);
  return !finalScore ?
    '' :
    finalScore[0].points > finalScore[1].points ?
      finalScore[0].team :
      finalScore[1].points > finalScore[0].points ?
        finalScore[1].team :
        ''  //tied
}


declare interface ITeamGamePointDifferential {
  [teamId: string]: number
}

/**
 * GET point differential of game mapped by team;
 * @param {IGame} game
 * @return {ITeamGamePointDifferential} - guaranteed for a map to be returned.
 */
export function getGamePointDifferential(game: IGame): ITeamGamePointDifferential {
  let results = {};
  let finalScore = getGameFinalScore(game);
  if (!game.final || !finalScore) {
    results[game.teams[0]] = 0;
    results[game.teams[1]] = 0;
    return results;
  }
  results[finalScore[0].team] = finalScore[0].points - finalScore[1].points;
  results[finalScore[1].team] = finalScore[1].points - finalScore[0].points;
  return results;


}

/**
 * GET final score of game. (does not check for game.final)
 * @param {IGame} game
 * @return {Array<{team: string; points: number}>}
 */
export function getGameFinalScore(game: IGame): Array<{ team: string, points: number }> {
  return _.get(_.last(game.scores), 'score');
}

/**
 * Determine whether game is finished or not.
 * @param {IGame} game
 * @return {boolean}
 */
export function gameIsFinished(game: IGame): boolean {
  return game.final;
}
