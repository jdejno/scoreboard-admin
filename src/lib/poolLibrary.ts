import _ from 'lodash';
import {IMatch, IPool} from '../../models/models';
import {matchIsFinished, matchIsScheduledNow} from './matchLibrary';

/**
 * Get the current match being played in the tournament. Does not factor in start times.
 * Must have IMatch populated within the pool for this function to work properly.
 * @param {IPool} pool
 * @return {IMatch} - Current Match.
 */
export function getPoolCurrentMatch(pool: IPool): IMatch {
  return _.find(pool.matches, !matchIsFinished)
}

/**
 * Get array of all finished matches within this pool.
 * @param {IPool} pool
 * @return {Array<IMatch>}
 */
export function getPoolFinishedMatches(pool: IPool): Array<IMatch> {
  return _.filter(pool.matches, matchIsFinished);
}

/**
 * Get array of all unfinished matches within this pool.
 * @param {IPool} pool
 * @return {Array<IMatch>}
 */
export function getPoolUnfinishedMatch(pool: IPool): Array<IMatch> {
  return _.filter(pool.matches, !matchIsFinished)
}

/**
 * Get the current match that is 'supposed' to be playing right now based on the start time.
 * Must have IMatch populated within the pool for this function to work properly.
 * @param {IPool} pool
 * @param {number} duration - duration of match in minutes
 * @return {IMatch}
 */
export function getPoolCurrentScheduledMatch(pool: IPool, duration: number = 60): IMatch {
  return _.find(pool.matches, matchIsScheduledNow.bind(this, pool, duration))
}



export function getPoolCurrentTeamRank(pool: IPool) {

}
