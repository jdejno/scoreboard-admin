import _ from 'lodash';
import {IGame, IMatch} from '../../models/models';
import {gameIsFinished, getGamePointDifferential, getGameWinner} from './gameLibrary';

/**
 * GET the match winner.
 * @param {IMatch} match
 * @return {string}
 */
export function getMatchWinner(match: IMatch): string {
  let games = _.filter(match.games, 'final'); //only get games with final scores
  let wins = _.countBy(games, getGameWinner); //count by game winner. returns { [teamId: string]: win count }
  let winner = _.maxBy(_.entries(wins), win => win[1]); //returns entries as ['id', 'points']
  return winner ? winner[0] : '' //return team id with most wins.
}

/**
 * GET wins and losses for a single team.
 * @param {IMatch} match
 * @param teamId {string} - team id of results to get.
 * @return {Array<string>} - results[0] = wins, results[1] = loses
 */
export function getMatchGameWinsAndLosesForTeam(match: IMatch, teamId: string): Array<number> {
  let games = _.filter(match.games, 'final'); //only get games with final scores
  let wins = _.countBy(games, getGameWinner); //count by game winner
  let losses = games.length - wins;
  return [wins, losses]
}

/**
 * Interface for wins and losses mapped by team id.
 */
declare interface ITeamWinsAndLossMap {
  [teamId: string]: Array<number>;
}

/**
 * GET wins and losses mapped by team id.
 * @param {IMatch} match
 * @return {ITeamWinsAndLossMap} - return[teamId][0] = wins, return[teamId][1] = losses
 */
export function getMatchGameWinsAndLossesTeamMap(match: IMatch): ITeamWinsAndLossMap {
  let games = _.filter(match.games, gameIsFinished); //only get games with final scores
  let wins = _.countBy(games, getGameWinner); //count by game winner. WARNING: will only return teamIds if team won at
                                              // least one game.
  let result = _.mapValues(wins, gameWins => [(gameWins || 0), games.length - (gameWins || 0)]);
  result[match.teams[0]] = result[match.teams[1]] ? _.reverse(_.clone(result[match.teams[1]])) : result[match.teams[0]];
  result[match.teams[1]] = result[match.teams[0]] ? _.reverse(_.clone(result[match.teams[0]])) : result[match.teams[1]];
  return result;
}

declare interface ITeamMatchPointDifferential {
  [teamId: string]: number
}

/**
 * GET point differential for the match.
 * @param {IMatch} match
 * @return {ITeamMatchPointDifferential}
 */
export function getMatchPointDifferentialTeamMap(match: IMatch): ITeamMatchPointDifferential {
  let games = _.filter(match.games, gameIsFinished); //only get games with final scores
  return _.reduce(games, (result, game) =>
      _.mapValues(getGamePointDifferential(game), (points, teamId) => (result[teamId] || 0) + points),
    {});
}

/**
 * Determine whether match is finished or not.
 * @param {IMatch} match
 * @return {boolean}
 */
export function matchIsFinished(match: IMatch): boolean {
  if (match instanceof Object)
    return getMatchCurrentGame(match) === void 0;
}

/**
 * Determine whether match is scheduled for the current time.
 * @param {IMatch} match
 * @param {number} duration
 * @return {boolean}
 */
export function matchIsScheduledNow(match: IMatch, duration: number = 60): boolean {
  let now = new Date();
  let end = new Date();
  end.setHours(end.getHours(), end.getMinutes() + duration);
  return match.scheduledTime >= now && match.scheduledTime <= end
}

/**
 * Determine whether this match is supposed to be playing or not right now.
 * @param {IMatch} match
 * @param {number} duration
 * @return {boolean}
 */
export function isMatchPlayingAsScheduled(match: IMatch, duration: number = 60): boolean {
  return !matchIsFinished(match) && matchIsScheduledNow(match, duration);
}

/**
 * Get the current game being played within the match.
 * @param {IMatch} match
 * @return {IGame|void}
 */
export function getMatchCurrentGame(match: IMatch): IGame {
  if (match instanceof Object)
    return _.find(match.games, !gameIsFinished)
}
