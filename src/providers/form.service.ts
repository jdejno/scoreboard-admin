import {Injectable} from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { IDivision, IMatch, IPool, ITeam, ITournament } from '../../models/models';
import _ from 'lodash';

@Injectable()
export class FormService {
  
  private _cacheMap: Map<string,any>;
  
  constructor(private fb:FormBuilder) {
    this._cacheMap = new Map<string,any>();
  }
  
  /**
   * Create a from for the tournament and cache form values.
   * @param {ITournament} tournament
   * @returns {FormGroup}
   */
  public createTournamentForm(tournament: ITournament): FormGroup {
    const status = tournament.status === 'ACTIVE';
    let form = this.fb.group({
      title: [tournament.title, Validators.required],
      status: [status, Validators.required],
      dates: this.fb.group({
        start: [_.min(tournament.dates), Validators.required],
        end: [_.max(tournament.dates), Validators.required],
      }),
      location: this.fb.group({
        name: [_.get(tournament, 'location.name'), Validators.required],
        city: [_.get(tournament, 'location.city'), Validators.required],
        state: [_.get(tournament, 'location.state'), Validators.required],
        address: _.get(tournament, 'location.address'),
      })
    });
    this._cacheMap.set(tournament._id, form.getRawValue());
    return form;
  }
  
  /**
   * Create a form group for the division and cache form values.
   * @param {IDivision} division
   * @returns {FormGroup}
   */
  public createDivisionFrom(division: IDivision): FormGroup {
    let form = new FormGroup({
      _id: new FormControl(division._id),
      gender: new FormControl(division.gender, Validators.required),
      level: new FormControl(division.level, Validators.required),
    });
    this._cacheMap.set(division._id, form.getRawValue());
    return form;
  }
  
  /**
   * Create form group from pool and cache form values.
   * @param {IPool} pool
   * @returns {FormGroup}
   */
  public createPoolForm(pool: IPool): FormGroup {
    let form = this.fb.group({
      _id: [_.get(pool, '_id'), Validators.required],
      title: [_.get(pool, 'title'), Validators.required],
      startTime: [_.get(pool, 'startTime'), ''],
      location: this.fb.group({
        location: [_.get(pool, 'location.location'), ''],
        court: [_.get(pool, 'location.court'), Validators.required],
      })
    });
    this._cacheMap.set(pool._id, form.getRawValue());
    return form;
  }
  
  /**
   * Create FormGroup from ITeam object.
   * @param {ITeam} team
   * @returns {FormGroup}
   */
  public createTeamForm(team: ITeam): FormGroup {
    let form = this.fb.group({
      _id: [team._id, Validators.required],
      title: [team.title, Validators.required]
    });
    this._cacheMap.set(team._id, form.getRawValue());
    return form;
  }
  
  /**
   * Create a FormGroup from an IMatch Object
   * @param {IMatch} match
   * @return {FormGroup}
   * @private
   */
  public createMatchForm(match: IMatch): FormGroup {
    return this.fb.group({
      _id: match._id,
      teams: this._createMatchTeamFormArray(match.teams),
      scheduledTime: [{value: match.scheduledTime, disabled: true}, Validators.required]
    });
  }
  
  
  /**
   * Check whether form needs updating or not by comparing the cached value to the compare form.
   * @param {string} id - id of the model with cached form values.
   * @param {FormGroup} form - form to compare to the cached value.
   * @returns {boolean}
   */
  public formNeedsUpdating(id: string, form: FormGroup): boolean {
    return !_.isEqual(this._cacheMap.get(id), form.getRawValue());
  }
  
  
  //#region private methods
  
  /**
   * Create a FormArray of team FormControls
   * @param {Array<ITeam>} teams
   * @return {FormArray}
   * @private
   */
  private _createMatchTeamFormArray(teams: Array<ITeam>): FormArray {
    return this.fb.array(_.map(teams, team => this.fb.group({
      _id: team._id,
      title: [{value: team.title, disabled: true}, '']
    })))
    
  }
  
  
  //#endregion
  
}
