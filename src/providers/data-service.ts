import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {IUserResponse} from './user-service';
import {IDivision, IPool, ITeam, ITournament} from '../../models/models';

@Injectable()
export class DataService {
  
  constructor(private _http: HttpClient) {}
  
  public authenticateUser(fbAccessToken: string): Observable<IUserResponse> {
    return this._postObject('/api/auth/facebook', {access_token: fbAccessToken});
  }
  
  //#region Tournament
  
  /**
   * GET all tournaments metadata (for use in list views)
   * @returns {Observable<any>}
   */
  public getTournamentsInfo(): Observable<any[]> {
    return this._getJson('/api/tournaments/metadata');
  }
  
  /**
   * GET tournament by id.
   * @param {string} id
   * @return {Observable<ITournament>}
   */
  public getTournament(id: string): Observable<ITournament> {
    return this._getJson('/api/tournaments/' + id);
  }
  
  /**
   * GET tournament with all models populated by id.
   * @param {string} id
   * @return {Observable<ITournament>}
   */
  public getTournamentAllData(id: string): Observable<ITournament> {
    return this._getJson(`/api/tournaments/${id}/full`);
  }
  
  /**
   * GET Tournament based on the user who created (or is a valid editor) of those tournaments
   * @param {string} userId
   * @return {Observable<ITournament>}
   */
  public getTournamentsByCreator(userId: string): Observable<Array<ITournament>> {
    return this._getJson('/api/tournaments/creator/' + userId);
  }
  
  /**
   * PUT to update tournament
   * @param {string} id - id of tournament
   * @param {ITournament} tournament - ITournament object to use for updating.
   * @return {Observable<ITournament>}
   */
  public updateTournament(id: string, tournament: ITournament): Observable<ITournament> {
    return this._putObject('/api/tournaments/' + id, tournament);
  }
  
  /**
   * Delete division from tournament
   * @note this will delete the division and all data within it.
   * @param {string} tournamentId
   * @param {string} divisionId
   * @returns {Observable<ITournament>}
   */
  public removeDivisionFromTournament(tournamentId: string, divisionId: string): Observable<ITournament> {
    return this._deleteObject(`/api/tournaments/${tournamentId}/remove/division/${divisionId}`);
  }
  
  //#endregion
  
  //#region Division
  
  /**
   * GET divisions for a single tournament
   * @desc Array of divisions will be populated with pools and matches.
   * @param {string} tournamentId
   * @return {Observable<Array<IDivision>>}
   */
  public getDivisionsByTournament(tournamentId: string): Observable<Array<IDivision>> {
    return this._getJson('/api/divisions/tournament/' + tournamentId);
  }
  
  /**
   * PUT to update division.
   * @param {string} id - id of division.
   * @param {IDivision} division - IDivision Object of changes.
   * @return {Observable<IDivision>}
   */
  public updateDivision(id: string, division: IDivision): Observable<IDivision> {
    return this._putObject('/api/divisions/' + id, division);
  }
  
  /**
   * Add new pool to division.
   * @param {string} divisionId - id of division to add pool too.
   * @param {IPool} pool -  pool to add.
   * @return {Observable<IPool>}
   */
  public addNewPoolToDivision(divisionId: string, pool: IPool): Observable<IPool> {
    return this._putObject(`/api/divisions/${divisionId}/add/pool`, {
      title: pool.title,
      startTime: pool.startTime,
      location: pool.location,
      format: pool.format
    });
  }
  
  /**
   * Remove a pool from division.
   * @param {string} divisionId
   * @param {string} poolId
   * @returns {Observable<IDivision>}
   */
  public removePoolFromDivision(divisionId: string, poolId: string): Observable<IDivision> {
    return this._putObject(`/api/divisions/${divisionId}/remove/pool`, {poolId: poolId});
  }
  
  //#endregion
  
  //#region Pool
  
  public getPool(id: string): Observable<IPool> {
    return this._getJson('/api/pools/' + id);
  }
  
  /**
   * PUT to update pool.
   * @param {string} id - id of pool to update.
   * @param {IPool} pool - pool data to update with
   * @return {Observable<IPool>}
   */
  public updatePool(id: string, pool: IPool): Observable<IPool> {
    return this._putObject('/api/pools/' + id, pool);
  }
  
  /**
   * Add team to pool
   * @param {string} poolId
   * @param data - team to add and id of tournament that is being updated.
   * @return {Observable<IPool>}
   */
  public addTeamToPool(poolId: string, data: {teamTitle: string, tournamentId: string}): Observable<IPool> {
    return this._postObject(`/api/pools/${poolId}/add/team`, {
      teamTitle: data.teamTitle,
      tournamentId: data.tournamentId
    })
  }
  
  /**
   * Remove team from pool.
   * @param {string} poolId - pool to remove team from.
   * @param {string} teamId - id of team to remove.
   * @return {Observable<IPool>}
   */
  public removeTeamFromPool(poolId: string, teamId: string): Observable<IPool> {
    return this._deleteObject(`/api/pools/${poolId}/remove/team/${teamId}`)
  }
  
  /**
   * Reorder the teams within a pool.
   * @param {string} poolId - id of pool to update.
   * @param {Array<string>} teamIds - array of team ids to use for reordering.
   * @return {Observable<IPool>}
   */
  public reorderTeamInPool(poolId: string, teamIds: Array<string>): Observable<IPool> {
    return this._postObject(`/api/pools/${poolId}/reorder/team`, {
      teamIds: teamIds,
    })
  }
  
  //#endregion
  
  //#region Team
  
  /**
   * Change the name of an existing team on the server.
   * @param {string} id - id of the team to update.
   * @param {string} teamTitle - title of the team.
   * @return {Observable<ITeam>}
   */
  public changeTeamName(id: string, teamTitle: string): Observable<ITeam> {
    return this._putObject('/api/teams/' + id, {
      title: teamTitle
    })
  }
  
  
  //#endregion
  
  //#region Private Helpers
  
  /**
   * Reuse this for all json GET requests
   * TODO: Store in Local Storage and get from there if exists
   * TODO: Add the FB access token for request auth once we have server side changes in
   */
  private _getJson(url: string): Observable<any> {
    return this._http.get(url);
  }
  
  /**
   * Reuse this for all object POST requests
   * @param url
   * @param object Object Should be validated before calling this method.
   * @returns {Observable<any>} - json of object posted.
   * TODO: Store in Local Storage on POST response
   * TODO: Add the FB access token for request auth once we have server side changes in
   */
  private _postObject(url: string, object: Object): Observable<any> {
    return this._http.post(url, object);
  }
  
  private _patchObject(url: string, object: Object): Observable<any> {
    return this._http.patch(url, object);
  }
  
  private _putObject(url: string, object: Object): Observable<any> {
    return this._http.put(url, object);
  }
  
  /**
   * Delete HTTP REST. Reuse in all DELETE Requests
   * @param {string} url
   * @param {Object} object
   * @returns {Observable<any>}
   * @private
   */
  private _deleteObject(url: string): Observable<any> {
    return this._http.delete(url);
  }
  
  //#endregion
  
}
