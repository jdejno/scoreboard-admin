import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Entry, File} from '@ionic-native/file';
import {FileTransfer, FileTransferObject} from '@ionic-native/file-transfer';
import {FileOpener} from '@ionic-native/file-opener';
import {Platform} from 'ionic-angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';


/**
 * Use to GET and POST files to the server.
 */
@Injectable()
export class FilesProvider {

  private _transfer: FileTransferObject;
  private _storageDirectory: string = '';

  constructor(public http: HttpClient,
              private _fileTransfer: FileTransfer,
              private _file: File,
              private _fileOpener: FileOpener,
              private _platform: Platform) {
    this._transfer = this._fileTransfer.create();
    this._cacheStorageLocation();
  }

  //#region Public Web Services

  /**
   * POST tournament data/file to server for new tournament creation.
   * @param {Object} data
   * @return {Observable<any>}
   */
  public postTournamentData(data: FormData): Observable<any> {
    return this._postFormData('/admin/api/files/tournament', data)
  }

  /**
   * Download and store tournament template csv.
   * @param {string} filename - name of the file.
   * @returns {Observable<boolean>} - result of saving the file.
   */
  public downloadTournamentTemplateCSV(filename: string): Observable<boolean> {
    return new Observable(observer => {
      this._getTournamentTemplateCsv().subscribe(
        file => this._storeFileLocally(file, filename).subscribe(
          success => observer.next(success),
          err => observer.error(err),
          () => observer.complete()
        ))
    });
  }

  //#endregion


  //#region Private Methods

  /**
   * Store file from Blob on the client using platform specific implementations for file storing.
   * @param {Blob} data - Blob of data
   * @param {string} filename - name of the file to download on the client.
   * @private
   * @note - If this is called in the DevApp, it will fail since this plugin is not supported...
   *  However, the
   */
  private _storeFileLocally(data: Blob, filename: string): Observable<boolean> {

    return Observable.fromPromise(new Promise((resolve, reject) => {
        this._platform.ready().then(
          () => {
            if (this._platform.is('core')) {
              return resolve(this._storeFileBrowser(data, filename));
            }
            else {
              this._storeFileNativeDevice(data, filename).then(
                entry => {
                  console.log('File created: ' + entry.nativeURL);
                  this._fileOpener.open(entry.nativeURL, data.type)
                    .then(res => {
                      console.log('File opened: ' + entry.name);
                      resolve(res)
                    })
                    .catch(err => reject(err))
                })
                .catch(err => reject(err))
            }
          }).catch(err => reject(err))
      })
    )
  }

  /**
   * Store blob of data to file locally using native device APIs.
   * @param {Blob} blob
   * @param {string} filename
   * @private
   */
  private _storeFileNativeDevice(blob: Blob, filename: string): Promise<Entry> {
    return this._file.writeFile(this._storageDirectory, filename, blob, {replace: true})
  }

  /**
   * Stores the file using browser window object. Use for running app in browser.
   * @param {Blob} blob - Blob of data to store.
   * @param {string} filename
   * @returns {boolean}
   * @private
   */
  private _storeFileBrowser(blob: Blob, filename: string): boolean {

    if (navigator.msSaveOrOpenBlob) {
      navigator.msSaveBlob(blob, filename);
    } else {
      let a = document.createElement('a');
      a.href = window.URL.createObjectURL(blob);
      a.download = filename;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      window.URL.revokeObjectURL(a.href);
    }
    return true
  }

  /**
   * GET csv template for populating tournament data.
   * @return {Observable<Blob>}
   */
  private _getTournamentTemplateCsv(): Observable<Blob> {
    return this._getText('/admin/api/files/tournament/template')
      .map(file => new Blob([file], {type: 'text/csv'}));
  }

  //#endregion

  //#region Private Helpers

  /**
   * GET text resource from server
   * @param {string} url
   * @return {Observable<string>}
   * @private
   */
  private _getText(url: string): Observable<string> {
    return this.http.get(url, {responseType: 'text'})
  }

  /**
   * Post Form Data
   * @param {string} url - url to post to.
   * @param data {FormData} - data to post.
   * @returns {Observable<any>}
   * @private
   */
  private _postFormData(url: string, data: FormData): Observable<any> {
    return this.http.post(url, data, {});
  }

  /**
   * Get the proper storage directory based on the platform:
   * ios - documents directory
   * android - data directory
   * other - null
   */
  private _cacheStorageLocation(): void {

    this._platform.ready().then(
      () => {
        if (this._platform.is('ios')) {
          this._storageDirectory = this._file.documentsDirectory;
          return
        }

        if (this._platform.is('android')) {
          this._storageDirectory = this._file.dataDirectory;
          return
        }
      }
    )
  }

  //#endregion
}
