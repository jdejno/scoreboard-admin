import {Injectable} from '@angular/core';
import {GlobalEventService, GlobalEventTopics} from './global-event-service';
import {LocalCacheKeys, LocalCacheService} from './local-cache-service';
import {Observable} from 'rxjs/Observable';
import {ITournament} from '../../models/models';

/**
 * Local cache service of globally set tournaments and subscribing to tournament related events.
 * @class
 */
@Injectable()
export class TournamentService {
  
  constructor(private _globalService: GlobalEventService,
              private _localCache: LocalCacheService) {}
  
  /**
   * Get local cache of globally set tournament.
   * @return {Observable<ITournament>}
   */
  public getTournament(): Observable<ITournament> {
    return this._localCache.get(LocalCacheKeys.Tournament);
  }
  
  /**
   * Set local cache of globally set tournament.
   * @param {ITournament} tournament
   */
  public setTournament(tournament: ITournament): Promise<void> {
    return new Promise((resolve, reject) => {
      this._localCache.set(LocalCacheKeys.Tournament, tournament)
        .do(() =>this._globalService.publish(GlobalEventTopics.TournamentSelected, tournament))
        .subscribe(() => resolve(), (err) => reject(err))
    });
  }
  
  /**
   * Update local cache of globally set tournament.
   * @param {ITournament} tournament
   */
  public updateTournament(tournament: ITournament): void {
    this._localCache.set(LocalCacheKeys.Tournament, tournament).subscribe(
      tournament => this._globalService.publish(GlobalEventTopics.TournamentUpdated, tournament)
    )
  }
  
  /**
   * Subscribe to changes to globally set tournament.
   * @param {Function} handlers
   */
  public subscribeTournamentSelected(...handlers: Array<Function>): void {
    this._globalService.subscribe(GlobalEventTopics.TournamentSelected, ...handlers);
  }
  
  /**
   * Subscribe to updates to globally set tournament.
   * @param {Function} handlers
   */
  public subscribeTournamentUpdated(...handlers: Array<Function>): void {
    this._globalService.subscribe(GlobalEventTopics.TournamentUpdated, ...handlers);
  }
  
}
