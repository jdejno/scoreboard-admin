import {Injectable} from '@angular/core';
import {DataService} from './data-service';
import {IDivision, IPool, ITournament} from '../../models/models';
import {Observable} from 'rxjs/Observable';
import _ from 'lodash';
import {getMatchGameWinsAndLossesTeamMap, getMatchPointDifferentialTeamMap, getMatchWinner} from '../lib/matchLibrary';
import {ReplaySubject} from 'rxjs/ReplaySubject';

//#region interfaces

//#region team results

export interface IDashboardTeamResult {
  title: string
  teamId: string
  matchWins: number
  matchLosses: number
  gameWins: number
  gameLosses: number
  pointDifferential: number
  rank: number
}

export interface IDashboardTeamResults {
  [teamId: string]: Array<IDashboardTeamResult>
}

export interface IDashboardDivisionResults extends IDivision {
  teamStats: IDashboardDivisionResults
}

//#endregion

//#region divisions

export interface IDashboardPoolSchedule extends IPool {

}

export interface IDashboardDivision extends IDivision {

}


//#endregion

//#endregion

@Injectable()
export class DashboardService {
  
  private _disposeObservable: ReplaySubject<void>;  //used to dispose observables.
  
  constructor(private _dataService: DataService) {}
  
  /**
   * GET Observable that returns an IDashBoardTeamResults object every 60 seconds.
   * @desc NOTE: You must call this.disposeTeamResultsSubject on ngDestroy (of equivalent event hook) to prevent memory leaks...
   * @param {string} tournamentId
   * @param {number} pollingInterval - polling interval
   * @return {Observable<IDashboardTeamResults>}
   */
  public initializeTeamResultsSubject(tournamentId: string, pollingInterval: number = 60000): Observable<Array<IDashboardDivisionResults>> {
    this._disposeObservable = new ReplaySubject<void>(1);
    return Observable.interval(pollingInterval).startWith(0)
      .takeUntil(this._disposeObservable)
      .switchMap(() => this._dataService.getTournamentAllData(tournamentId)
        .map(this._transformToDashboardTeamResults.bind(this))
      )
  }
  
  
  /**
   * Clean up any subscriptions we acquired using 'takeUntil'
   */
  public disposeSubjects(): void {
    if(this._disposeObservable) {
      this._disposeObservable.next(undefined);
      this._disposeObservable.complete();
    }
  }
  
  //#region private helpers
  
  /**
   * Transform ITournament object into a usable results model.
   * @param {ITournament} tournament
   * @private
   */
  private _transformToDashboardTeamResults(tournament: ITournament): Array<IDashboardDivisionResults> {
    let results = [];
    _.each(tournament.divisions, division => {
      let teamNameMap = _.keyBy(division.teams, '_id');
      let teamStats = {};
      division.teamStats = teamStats;
      results.push(division);
      
      _.each(division.pools, pool => {
        
        _.each(pool.matches, match => {
          let winner = getMatchWinner(match);
          let hasWinner = !!winner;
          let loser;
          if (hasWinner) {
            loser = _.first(_.without(match.teams, winner));
          } else {
            winner = match.teams[0];
            loser = match.teams[1];
          }
          
          let matchGameResults = getMatchGameWinsAndLossesTeamMap(match);
          let pointDiffMap = getMatchPointDifferentialTeamMap(match);
          teamStats = this._assignTeamResultObject(teamStats, winner, teamNameMap[winner].title);
          teamStats = this._assignTeamResultObject(teamStats, loser, teamNameMap[loser].title);
          
          if (hasWinner) {
            teamStats[winner]['matchWins']++;
            teamStats[winner]['gameWins'] += matchGameResults[winner][0];
            teamStats[winner]['gameLosses'] += matchGameResults[winner][1];
            teamStats[winner]['pointDifferential'] += pointDiffMap[winner];
            
            teamStats[loser]['matchLosses']++;
            teamStats[loser]['gameWins'] += matchGameResults[loser][0];
            teamStats[loser]['gameLosses'] += matchGameResults[loser][1];
            teamStats[loser]['pointDifferential'] += pointDiffMap[loser];
          }
        })
      })
    });
    results = _.map(results, this._rankTeamResults);
    return results;
  }
  
  private _assignTeamResultObject(results: Object, team: string, teamTitle: string) {
    results[team] = results[team] || {teamId: team, title: teamTitle};
    results[team]['matchWins'] = results[team]['matchWins'] || 0;
    results[team]['matchLosses'] = results[team]['matchLosses'] || 0;
    results[team]['gameWins'] = results[team]['gameWins'] || 0;
    results[team]['gameLosses'] = results[team]['gameLosses'] || 0;
    results[team]['pointDifferential'] = results[team]['pointDifferential'] || 0;
    return results;
  }
  
  
  private _rankTeamResults(results: IDashboardDivisionResults): IDashboardDivisionResults {
    //TODO: Handle head to head tie breaks. Currently, very crude....
    results.teamStats = _.orderBy(results.teamStats, ['pointDifferential', 'matchLosses', 'matchWins'], ['desc', 'asc', 'desc']);
    results.teamStats = _.mapValues(results.teamStats, (stats, rank) => {
      stats['rank'] = +rank+1;
      return stats;
    });
    return results;
  }
  
  //#endregion
  
}
