import {Component, Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';


export interface ISidebarComponent extends Component {
  initialize(): void
  refresh(): void
}

export enum SidebarTypes {
  TournamentSelection
}

@Injectable()
export class SidebarService {
  
  //private members
  private _sidebarIsShowing: boolean;
  private _currSidebarComponent: SidebarTypes;
  
  //subjects
  private _refreshSubject: Subject<void>;
  private _sidebarSetSubject: Subject<SidebarTypes>;
  private _destroySidebarSubject: Subject<void>;
  
  //#region Ctor
  
  constructor() {
    this._refreshSubject = new Subject<void>();
    this._sidebarSetSubject = new Subject<SidebarTypes>();
    this._destroySidebarSubject = new Subject<void>();
  }
  
  //#endregion
  
  //#region public methods
  
  /**
   * Set and show a sidebar.
   * @param {SidebarTypes} sidebar
   */
  public initializeSidebar(sidebar: SidebarTypes): void {
    this.setSidebarComponent(sidebar);
    this.showSidebar();
  }
  
  /**
   * Set the sidebar component from the list of enums [SidebarTypes]
   * @param {SidebarTypes} sidebar
   */
  public setSidebarComponent(sidebar: SidebarTypes) {
    if(this._currSidebarComponent !== sidebar) {
      this._sidebarSetSubject.next(sidebar);
      this._currSidebarComponent = sidebar;
    }
  }
  
  /**
   * Get the sidebarSubject to subscribe to.
   * @return {Observable<SidebarTypes>}
   */
  public getSetSidebarSubject(): Observable<SidebarTypes> {
    return this._sidebarSetSubject.asObservable();
  }
  
  /**
   * Get the refreshSubject to subscribe to.
   * @return {Observable<void>}
   */
  public getRefreshSidebarSubject(): Observable<void> {
    return this._refreshSubject.asObservable();
  }
  
  /**
   * Get the destroySubject to subscribe to.
   * @return {Observable<void>}
   */
  public getDestroySidebarSubject(): Observable<void> {
    return this._destroySidebarSubject.asObservable();
  }
  
  /**
   * Refresh the current sidebar(s) via publishing through our refreshSubject.
   */
  public refreshSidebar() {
    this._refreshSubject.next();
  }
  
  /**
   * Destroy all sidebar components
   */
  public destroySidebar() {
    this._destroySidebarSubject.next();
    this._destroySidebarSubject.complete();
  }
  
  /**
   * Show the current sidebar if one is currently rendered in SidebarManager
   */
  public showSidebar(): void {
    this._sidebarIsShowing = true;
  }
  
  /**
   * Hide the sidebar
   */
  public hideSidebar(): void {
    this._sidebarIsShowing = false;
  }
  
  /**
   * Whether or not sidebar is currently showing.
   * @return {boolean}
   */
  public sidebarIsShowing(): boolean {
    return this._sidebarIsShowing;
  }
  
  //#nedregion
  
}
