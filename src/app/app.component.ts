import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {Observable} from "rxjs/Observable";
import {IUserResponse, UserService} from "../providers/user-service";
import {SocialService} from "../providers/social-service";
import {LoginPage} from "../pages/login/login";
import {GlobalEventService, GlobalEventTopics} from "../providers/global-event-service";
import {HomeSidebarComponent} from '../components/sidebar/home-sidebar.component';
import {SidebarService, SidebarTypes} from '../providers/sidebar-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @ViewChild(HomeSidebarComponent) sidebar: HomeSidebarComponent;

  constructor(private _globalEventService: GlobalEventService,
              public platform: Platform,
              private _socialService: SocialService,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              private _sidebarService: SidebarService,
              private _userService: UserService) {

    platform.ready().then(() => {

      // Listen for logout.
      this._globalEventService.subscribe(GlobalEventTopics.UserLogout, this._handleLogoutEvent.bind(this));
      this._globalEventService.subscribe(GlobalEventTopics.UserLogin, this._handleLoginEvent.bind(this));
      

      //Check the login status and deal with this based on that.
      this._socialService.checkFbLoginStatus().then((res) => {
        if (res.status === 'connected') {
          // Good news. We are connected.
          this._userService.authenticateUser(res.authResponse.accessToken)
            .catch((err) => {
              this._openLoginPage();
              return Observable.throw(err);
            })
            .subscribe((response: IUserResponse) => {
              this._enterApp();
            });
        } else {
          // What happens if there is no auth or available connection? Bring to login for now...
          this._openLoginPage();
        }
      }).catch((err) => {
        this._openLoginPage();
      });

      statusBar.styleDefault();
    });
  }
  
  /**
   * Determine whether to show the sidebar or not.
   * @return {boolean}
   */
  public showSidebar(): boolean {
    return this._sidebarService.sidebarIsShowing();
  }

  /**
   * Enter the application. Should only be called once we are authenticated.
   * @private
   */
  private _enterApp() {
    this.nav.setRoot(HomePage, {}, {animate: true, direction: 'forward'});
    this.splashScreen.hide();
    this._sidebarService.initializeSidebar(SidebarTypes.TournamentSelection);
  }

  /**
   * Navigates to the login page
   * @private
   */
  private _openLoginPage() {
    this.nav.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});
  }
  
  /**
   * Handle event when user logs out.
   * - Opens the log in page.
   * - Hides the sidebar.
   * @private
   */
  private _handleLogoutEvent(){
    this._openLoginPage();
    this._sidebarService.hideSidebar();
    this._sidebarService.destroySidebar();
  }
  
  /**
   * Handle event when user logs in.
   * - Show the sidebar menu.
   * @private
   */
  private _handleLoginEvent() {
    this._sidebarService.initializeSidebar(SidebarTypes.TournamentSelection);
  }
  
}

