import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {File} from '@ionic-native/file';

import {MyApp} from './app.component';
import {ImportPage} from '../pages/import/import';
import {ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FileTransfer} from '@ionic-native/file-transfer';
import {FileOpener} from '@ionic-native/file-opener';
import {DataService} from '../providers/data-service';
import {StringHelpers} from '../helpers/string-helpers';
import {HomePage} from '../pages/home/home';
import {HomePageModule} from '../pages/home/home.module';
import {ImportPageModule} from '../pages/import/import.module';
import {DashboardPageModule} from '../pages/dashboard/dashboard.module';
import {PipeModule} from '../pipes/pipe.module';
import {ComponentsModule} from '../components/components.module';
import {LocalCacheService} from "../providers/local-cache-service";
import {TokenHelper, UserService} from "../providers/user-service";
import {SocialService} from "../providers/social-service";
import {Facebook} from "@ionic-native/facebook";
import {GlobalEventService} from "../providers/global-event-service";
import {IonicStorageModule} from "@ionic/storage";
import {LoginPage} from "../pages/login/login";
import {RequestInterceptor} from "../helpers/request-interceptor";
import {HomeSidebarComponent} from '../components/sidebar/home-sidebar.component';
import {SidebarService} from '../providers/sidebar-service';
import {TournamentService} from '../providers/tournament-service';
import {SidebarComponentManager} from '../components/sidebar/sidebar-manager.component';
import {FormService} from '../providers/form.service';
import { TestPage } from '../pages/test/test';
import { TestPageModule } from '../pages/test/test.module';
import { SocketIoConfig, SocketIoModule } from 'ng-socket-io';
import { ENV } from '@app/env';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomeSidebarComponent,
    SidebarComponentManager
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    HomePageModule,
    ImportPageModule,
    DashboardPageModule,
    ComponentsModule,
    PipeModule,
    TestPageModule,
    IonicStorageModule.forRoot({
      name: '__scoreboardDb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    SocketIoModule.forRoot(ENV.socketio as SocketIoConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ImportPage,
    HomePage,
    LoginPage,
    HomeSidebarComponent,
    TestPage
  ],
  providers: [
    DataService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Facebook,
    File,
    FileTransfer,
    FileOpener,
    GlobalEventService,
    {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true},
    LocalCacheService,
    StringHelpers,
    SocialService,
    SplashScreen,
    StatusBar,
    TokenHelper,
    UserService,
    SidebarService,
    TournamentService,
    FormService
  ]
})
export class AppModule {
}
