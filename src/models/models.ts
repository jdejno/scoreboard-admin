/**
 * Data Models
 */

export interface IUser {
  _id: string,
  name: string,
  firstName: string,
  lastName: string,
  email: string,
  image: string,
  gender: string,
  sports: Array<string>,
  facebookInfo: any,
  memberships: Array<any>
}
