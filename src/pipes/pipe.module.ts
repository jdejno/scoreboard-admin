import {NgModule} from '@angular/core';
import {DateRangeDisplayPipe, DelimPipe, GetPipe, InfinityPipe, KeysPipe} from './globals';

@NgModule({
  declarations: [
    KeysPipe,
    InfinityPipe,
    DateRangeDisplayPipe,
    GetPipe,
    DelimPipe
  ],
  exports: [
    KeysPipe,
    InfinityPipe,
    DateRangeDisplayPipe,
    GetPipe,
    DelimPipe
  ]
})
export class PipeModule {
  static forRoot() {
    return {
      ngModule: PipeModule,
      providers: [],
    };
  }
}
